package li.raphael.configuration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import li.raphael.configuration.ConfigurationFactory;
import li.raphael.configuration.ValueLoader;
import li.raphael.configuration.loader.AnnotationConfigurationLoader;
import li.raphael.configuration.loader.MapConfigurationLoader;
import li.raphael.configuration.loader.MethodNameConfigurationLoader;
import li.raphael.configuration.loader.PropertiesConfigurationLoader;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestConfigMaker {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void doTest() {
		ExampleConfig anExampleConfig = ConfigurationFactory.create(ExampleConfig.class);
		Assert.assertEquals("From the file", anExampleConfig.getString());
		Assert.assertEquals("Hello World!", anExampleConfig.getStringObject());

	}

	@Test
	public void testChain() throws IOException {
		ConfigurationFactory aConfigurationFactory = new ConfigurationFactory();

		List<ValueLoader> aChain = new ArrayList<>();
		aChain.add(new MethodNameConfigurationLoader());
		aConfigurationFactory.setConfigurationLoaderChain(aChain);

		ExampleConfig anExampleConfig = aConfigurationFactory.createConfig(ExampleConfig.class);
		Assert.assertEquals("getString", anExampleConfig.getString());

		aChain = new ArrayList<>();

		aChain.add(new AnnotationConfigurationLoader());
		aChain.add(new MethodNameConfigurationLoader());

		aConfigurationFactory.setConfigurationLoaderChain(aChain);
		anExampleConfig = aConfigurationFactory.createConfig(ExampleConfig.class);
		Assert.assertEquals("Hello World!", anExampleConfig.getString());

		aChain = new ArrayList<>();

		aChain.add(new PropertiesConfigurationLoader());
		aChain.add(new AnnotationConfigurationLoader());
		aChain.add(new MethodNameConfigurationLoader());

		aConfigurationFactory.setConfigurationLoaderChain(aChain);
		anExampleConfig = aConfigurationFactory.createConfig(ExampleConfig.class);
		Assert.assertEquals("From the file", anExampleConfig.getString());

		File aTemporaryBaseDirectory = createLocalExampleFile(ExampleConfig.class, "getString", "From FS");

		aChain = new ArrayList<>();
		aChain.add(new PropertiesConfigurationLoader(aTemporaryBaseDirectory.getAbsolutePath()));
		aChain.add(new PropertiesConfigurationLoader());
		aChain.add(new AnnotationConfigurationLoader());
		aChain.add(new MethodNameConfigurationLoader());

		aConfigurationFactory.setConfigurationLoaderChain(aChain);
		anExampleConfig = aConfigurationFactory.createConfig(ExampleConfig.class);
		Assert.assertEquals("From FS", anExampleConfig.getString());

	}

	private File createLocalExampleFile(Class<?> inClassToLoad, String inKey, String inValue) throws IOException {
		// Define temporary directory...
		File aTemporaryDirectory = File.createTempFile(getClass().getName(), null);
		aTemporaryDirectory.delete();

		// Define the properties file to load
		File aPropertiesFile = new File(aTemporaryDirectory, inClassToLoad.getName().replace(".", "/") + ".properties");

		// Create file structure
		aPropertiesFile.getParentFile().mkdirs();

		// Write test data...
		BufferedWriter aBufferedWriter = new BufferedWriter(new FileWriter(aPropertiesFile));
		aBufferedWriter.write(inKey);
		aBufferedWriter.write("=");
		aBufferedWriter.write(inValue);
		aBufferedWriter.flush();
		aBufferedWriter.close();

		// Clean up afterwards...
		aPropertiesFile.deleteOnExit();
		return aTemporaryDirectory;
	}

	@Test
	public void testValueLoading() throws IOException {

		ConfigurationFactory aConfigurationFactory = new ConfigurationFactory();
		List<ValueLoader> aValueLoaderChain = new ArrayList<>();
		HashMap<String, String> aValueMap = new HashMap<>();

		File aFile = new File(folder.getRoot(), "test1");
		aFile.createNewFile();
		
		aValueMap.put("getFile", aFile.getAbsolutePath());
		aValueMap.put("getFileUsingProtocoll", aFile.toURI().toString());
		
		
		aValueLoaderChain.add(new MapConfigurationLoader(aValueMap));
		aValueLoaderChain.add(new AnnotationConfigurationLoader());
		aConfigurationFactory.setConfigurationLoaderChain(aValueLoaderChain);

		// Register new value retriever
		ExampleConfig anExampleConfig = aConfigurationFactory.createConfig(ExampleConfig.class);
		Assert.assertEquals(new Integer(1), anExampleConfig.getInteger());
		Assert.assertEquals(new Double(1), anExampleConfig.getDouble());

		Assert.assertNotNull(anExampleConfig.getFile());
		Assert.assertNotNull(anExampleConfig.getFileUsingProtocoll());
	}
}
