package li.raphael.configuration.core;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import li.raphael.configuration.ConvertWith;
import li.raphael.configuration.LoadWith;
import li.raphael.configuration.ValueLoader;
import li.raphael.configuration.ValueRetriever;

public class ConfigurationInvocationHandler implements InvocationHandler {

	private List<ValueLoader> loaderChain;

	private Set<ValueRetriever> valueRetrievers;

	public ConfigurationInvocationHandler(List<ValueLoader> inLoaderChain, Set<ValueRetriever> inValueRetrievers) {
		loaderChain = inLoaderChain;
		valueRetrievers = inValueRetrievers;
	}

	@Override
	public Object invoke(Object inProxy, Method inMethod, Object[] inArgs) throws Throwable {
		// Load from defined loader if annotation is present
		if (inMethod.getDeclaringClass().isAnnotationPresent(LoadWith.class)) {
			Class<? extends ValueLoader> aValue = inMethod.getDeclaringClass().getAnnotation(LoadWith.class).value();
			ValueLoader aValueLoader = aValue.newInstance();
			return convertValue(aValueLoader, inProxy, inMethod, inArgs);
		}

		// Loop through value chain if not annotated.
		for (ValueLoader aValueLoader : loaderChain) {
			Object aLoadedValue = convertValue(aValueLoader, inProxy, inMethod, inArgs);

			// Return the value if it's not null
			if (aLoadedValue != null) {
				return aLoadedValue;
			}
		}

		return null;

	}

	private Object convertValue(ValueLoader inValueLoader, Object inProxy, Method inMethod, Object[] inArgs)
			throws Exception {

		// Load the value
		String aLoadedValue = inValueLoader.loadValue(inProxy.getClass(), inMethod);
		if (aLoadedValue == null) {
			return null;
		}

		// If a specific value retriever is defiend.
		if (inMethod.isAnnotationPresent(ConvertWith.class)) {
			Class<? extends ValueRetriever> aClass = inMethod.getAnnotation(ConvertWith.class).value();
			ValueRetriever aValueRetriever = aClass.newInstance();
			return aValueRetriever.getValue(aLoadedValue, inMethod, inArgs);
		}

		// Get all value retrievers which can be applied
		Map<Integer, ValueRetriever> aMatchingRetrievers = new HashMap<>();
		for (ValueRetriever aValueRetriever : valueRetrievers) {
			int aPriority = aValueRetriever.isAppliable(aLoadedValue, inMethod, inArgs);
			// Ignore values below 0
			if (aPriority < 0) {
				continue;
			}
			// Prevent duplicate priorities
			if (aMatchingRetrievers.containsKey(aPriority)) {
				throw new Exception("Duplicate priority!");
			}
			aMatchingRetrievers.put(aPriority, aValueRetriever);
		}

		// If only one entry matches
		if (aMatchingRetrievers.size() == 1) {
			ValueRetriever aValueRetriever = aMatchingRetrievers.values().iterator().next();
			return aValueRetriever.getValue(aLoadedValue, inMethod, inArgs);
		}

		Vector<Integer> aVector = new Vector<>(aMatchingRetrievers.keySet());
		Collections.sort(aVector);
		ValueRetriever aValueRetriever = aMatchingRetrievers.get(aVector.get(0));
		return aValueRetriever.getValue(aLoadedValue, inMethod, inArgs);
	}
}
