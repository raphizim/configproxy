package li.raphael.configuration.retrievers;

import java.lang.reflect.Method;

import li.raphael.configuration.ValueRetriever;

public class NumericValueRetriever implements ValueRetriever {

	@Override
	public Object getValue(String inObject, Method inMethod, Object[] inArgs) {
		return getValue(inObject, inMethod);
	}

	@Override
	public int isAppliable(String inObject, Method inMethod, Object[] inArgs) {
		try {
			Number aValue = getValue(inObject, inMethod);
			return aValue != null ? 1 : -1;
		} catch (Throwable aThrowable) {
			return -1;
		}
	}

	private Number getValue(String inObject, Method inMethod) {
		if (Integer.class.equals(inMethod.getReturnType())) {
			return Integer.parseInt(inObject);
		} else if (Double.class.equals(inMethod.getReturnType())) {
			return Double.parseDouble(inObject);
		} else if (Float.class.equals(inMethod.getReturnType())) {
			return Float.parseFloat(inObject);
		} else if (Short.class.equals(inMethod.getReturnType())) {
			return Short.parseShort(inObject);
		} else if (Long.class.equals(inMethod.getReturnType())) {
			return Long.parseLong(inObject);
		} else if (Byte.class.equals(inMethod.getReturnType())) {
			return Byte.parseByte(inObject);
		}
		return null;
	}
}
