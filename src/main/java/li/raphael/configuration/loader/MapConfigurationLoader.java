package li.raphael.configuration.loader;

import java.lang.reflect.Method;
import java.util.Map;

import li.raphael.configuration.ValueLoader;

public class MapConfigurationLoader implements ValueLoader {

	private Map<String, String> map;

	public MapConfigurationLoader(Map<String, String> inMap) {
		super();
		map = inMap;
	}

	@Override
	public String loadValue(Class<?> inClass, Method inKey) throws Exception {
		return map.get(inKey.getName());
	}

}
