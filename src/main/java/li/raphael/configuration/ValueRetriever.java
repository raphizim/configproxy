package li.raphael.configuration;

import java.lang.reflect.Method;

public interface ValueRetriever {

	Object getValue(String inObject, Method inMethod, Object[] inArgs) throws Exception;

	int isAppliable(String inObject, Method inMethod, Object[] inArgs) throws Exception;

}
