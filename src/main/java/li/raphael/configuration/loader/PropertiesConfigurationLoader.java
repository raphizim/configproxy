package li.raphael.configuration.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

import li.raphael.configuration.ValueLoader;

public class PropertiesConfigurationLoader implements ValueLoader {

	private String basePath;

	public PropertiesConfigurationLoader() {
		super();
	}

	public PropertiesConfigurationLoader(String inBasePath) {
		super();
		basePath = inBasePath;
	}

	@Override
	public String loadValue(Class<?> inClass, Method inKey) throws Exception {
		Properties aProperties = new Properties();
		StringBuilder aPath = new StringBuilder();

		// TODO: make loading of the name save...
		aPath.append(inClass.getInterfaces()[0].getName().replace(".", "/"));

		aPath.append(".properties");
		if (basePath != null) {
			File aFile = new File(basePath, aPath.toString());
			if (aFile.exists() && aFile.canRead()) {
				InputStream aResourceStream = new FileInputStream(aFile);
				aProperties.load(aResourceStream);
			}
		} else {
			aPath.insert(0, '/');
			InputStream aResourceStream = getClass().getResourceAsStream(aPath.toString());
			if (aResourceStream != null) {
				aProperties.load(aResourceStream);
			}
		}

		return aProperties.getProperty(inKey.getName());

	}

}
