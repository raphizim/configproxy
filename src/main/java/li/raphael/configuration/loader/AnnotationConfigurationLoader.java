package li.raphael.configuration.loader;

import java.lang.reflect.Method;

import li.raphael.configuration.DefaultValue;
import li.raphael.configuration.ValueLoader;

public class AnnotationConfigurationLoader implements ValueLoader {

	@Override
	public String loadValue(Class<?> inClass, Method inMethod) throws Exception {
		if (inMethod.isAnnotationPresent(DefaultValue.class)) {
			return inMethod.getAnnotation(DefaultValue.class).value();
		}
		return null;
	}

}
