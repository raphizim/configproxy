package li.raphael.configuration.retrievers;

import java.lang.reflect.Method;

import li.raphael.configuration.ValueRetriever;

public class StringValueRetriever implements ValueRetriever {

	@Override
	public Object getValue(String inObject, Method inMethod, Object[] inArgs) {
		return inObject;
	}

	@Override
	public int isAppliable(String inObject, Method inMethod, Object[] inArgs) {
		if (inMethod.getReturnType().isAssignableFrom(String.class)) {
			return 0;
		}
		return -1;
	}

}
