package li.raphael.configuration;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import li.raphael.configuration.core.ConfigurationInvocationHandler;
import li.raphael.configuration.loader.AnnotationConfigurationLoader;
import li.raphael.configuration.loader.MethodNameConfigurationLoader;
import li.raphael.configuration.loader.PropertiesConfigurationLoader;
import li.raphael.configuration.retrievers.FileInputStreamValueRetriever;
import li.raphael.configuration.retrievers.NumericValueRetriever;
import li.raphael.configuration.retrievers.StringValueRetriever;

public class ConfigurationFactory {

	/**
	 * The singleton instance.
	 */
	private static ConfigurationFactory instance;

	/**
	 * A set of value retrievers to retrieve values from a loaded string.
	 */
	private Set<ValueRetriever> valueRetrievers;

	/**
	 * The chain containing value loaders. The loading process will start with
	 * the first one (index 0) and calls the loadValue method on it. It will
	 * proceed with the next one if the returned value is null and so forth.
	 */
	private List<ValueLoader> loaderChain;

	/**
	 * Static method to access
	 * 
	 * @param inInterface
	 * @return
	 */
	public static <M> M create(Class<M> inInterface) {
		return getDefaultInstance().createConfig(inInterface);
	}

	public <M> M createConfig(Class<M> inInterface) {

		ClassLoader aClassLoader = inInterface.getClassLoader();

		ConfigurationInvocationHandler anInvocationHandler = new ConfigurationInvocationHandler(loaderChain,
				valueRetrievers);
		Object aProxyInstance = Proxy.newProxyInstance(aClassLoader, new Class[] { inInterface }, anInvocationHandler);

		return inInterface.cast(aProxyInstance);
	}

	/**
	 * Registers the given value retriever. The value retriever then will be
	 * used for 
	 * 
	 * @param inRetriever
	 */
	public void registerValueRetriever(ValueRetriever inRetriever) {
		valueRetrievers.add(inRetriever);
	}

	/**
	 * Removes the given retriever from the set of value retrivers.
	 * 
	 * @param inRetriever
	 *            the value retriever to remove.
	 */
	public void removeValueRetriever(ValueRetriever inRetriever) {
		valueRetrievers.add(inRetriever);
	}

	/**
	 * Removes all value retrievers.
	 */
	public void clearValueRetrievers() {
		valueRetrievers.clear();
	}

	public void setConfigurationLoaderChain(List<ValueLoader> inChain) {
		loaderChain = inChain;
	}

	/**
	 * Returns the default singleton instance of the ConfigurationFactory.
	 * 
	 * @return the singleton instance.
	 */
	private static ConfigurationFactory getDefaultInstance() {
		if (instance == null) {
			instance = new ConfigurationFactory();
			instance.setConfigurationLoaderChain(getDefaultValueLoaderChain());
		}
		return instance;
	}

	/**
	 * Creates the default value chain. This will load confgurations in the
	 * following order:
	 * <ul>
	 * <li>A properties file from the class path (see
	 * PropertiesConfigurationLoader)</li>
	 * <li>The value defined with the @DefaultValue annotation.</li>
	 * <li>The name of the method</li>
	 * 
	 * @return the default value chain.
	 */
	private static List<ValueLoader> getDefaultValueLoaderChain() {
		List<ValueLoader> outLoaderChain = new ArrayList<>();
		outLoaderChain.add(new PropertiesConfigurationLoader());
		outLoaderChain.add(new AnnotationConfigurationLoader());
		outLoaderChain.add(new MethodNameConfigurationLoader());
		return outLoaderChain;
	}

	public ConfigurationFactory() {
		// Register default value retrievers
		valueRetrievers = new HashSet<>();
		valueRetrievers.add(new StringValueRetriever());
		valueRetrievers.add(new NumericValueRetriever());
		valueRetrievers.add(new FileInputStreamValueRetriever());
	}

}
