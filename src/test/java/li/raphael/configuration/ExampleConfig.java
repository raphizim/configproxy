package li.raphael.configuration;

import java.io.FileInputStream;

import li.raphael.configuration.DefaultValue;
import li.raphael.configuration.retrievers.NumericValueRetriever;

public interface ExampleConfig {

	@DefaultValue("Hello World!")
	String getString();

	@DefaultValue("Hello World!")
	Object getStringObject();

	@ConvertWith(NumericValueRetriever.class)
	@DefaultValue("1")
	Integer getInteger();

	@DefaultValue("1")
	Double getDouble();

	FileInputStream getFile();
	
	FileInputStream getFileUsingProtocoll();
	
	// @DefaultValue("1")
	// Integer getInteger();
	//
	// @DefaultValue("file:///home/rzi/ZIVI")
	// InputStream getInputstream();
}
