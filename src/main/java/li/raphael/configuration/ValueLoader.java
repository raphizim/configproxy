package li.raphael.configuration;

import java.lang.reflect.Method;

public interface ValueLoader {

	String loadValue(Class<?> inClass, Method inKey) throws Exception;

}
