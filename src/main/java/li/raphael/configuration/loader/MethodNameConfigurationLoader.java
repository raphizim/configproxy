package li.raphael.configuration.loader;

import java.lang.reflect.Method;

import li.raphael.configuration.ValueLoader;

public class MethodNameConfigurationLoader implements ValueLoader {

	@Override
	public String loadValue(Class<?> inClass, Method inKey) throws Exception {
		return inKey.getName();
	}

}
