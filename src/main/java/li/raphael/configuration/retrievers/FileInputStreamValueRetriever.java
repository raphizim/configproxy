package li.raphael.configuration.retrievers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;

import li.raphael.configuration.ValueRetriever;

public class FileInputStreamValueRetriever implements ValueRetriever {

	@Override
	public Object getValue(String inObject, Method inMethod, Object[] inArgs) throws FileNotFoundException,
			URISyntaxException {
		if (inObject.startsWith("file:/")) {
			URI anUri = new URI(inObject);
			return new FileInputStream(new File(anUri));
		}
		return new FileInputStream(inObject);
	}

	@Override
	public int isAppliable(String inObject, Method inMethod, Object[] inArgs) {
		if (inMethod.getReturnType().isAssignableFrom(FileInputStream.class)) {
			if (inObject.startsWith("file:/") || new File(inObject).exists()) {
				return 2;
			}
			return 1;
		}
		return -1;
	}

}
